package cnps.dsi.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
public class MvStk {

    public static final int ENTREE = 1;

    public static final int SORTIE = 2;

    @Id
    @GeneratedValue
    private Long idMvtStk;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMvt;

    private BigDecimal quantite;

    private int typeMvt;

    @ManyToOne
    @JoinColumn(name = "idArticle")
    private  Article article;

    public static int getENTREE() {
        return ENTREE;
    }

    public static int getSORTIE() {
        return SORTIE;
    }

    public Long getIdMvtStk() {
        return idMvtStk;
    }

    public void setIdMvtStk(Long idMvtStk) {
        this.idMvtStk = idMvtStk;
    }

    public Date getDateMvt() {
        return dateMvt;
    }

    public void setDateMvt(Date dateMvt) {
        this.dateMvt = dateMvt;
    }

    public BigDecimal getQuantite() {
        return quantite;
    }

    public void setQuantite(BigDecimal quantite) {
        this.quantite = quantite;
    }

    public int getTypeMvt() {
        return typeMvt;
    }

    public void setTypeMvt(int typeMvt) {
        this.typeMvt = typeMvt;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public MvStk() {
    }
}
