package cnps.dsi.entities;


import javax.persistence.*;
import java.io.Serializable;

@Entity
public class LigneCommandeClient implements Serializable {

    @Id
    @GeneratedValue
    private Long idLigneCdeClt;

    @ManyToOne
    @JoinColumn(name = "idArticle")
    private Article article;

    @ManyToOne
    @JoinColumn(name = "idCommandeClient")
    private CommandeClient commandeClient;

    public Long getIdLigneCdeClt() {
        return idLigneCdeClt;
    }

    public void setIdLigneCdeClt(Long idLigneCdeClt) {
        this.idLigneCdeClt = idLigneCdeClt;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public CommandeClient getCommandeClient() {
        return commandeClient;
    }

    public void setCommandeClient(CommandeClient commandeClient) {
        this.commandeClient = commandeClient;
    }
}
