package cnps.dsi.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
public class Vente implements Serializable {

    @Id
    @GeneratedValue
    private Long idVente;

    private String code;

    @Temporal(TemporalType.TIMESTAMP)
    private Date datVente;

    @OneToMany(mappedBy = "vente")
    private List<LigneVente> ligneVentes;

    public Long getIdVente() {
        return idVente;
    }

    public void setIdVente(Long idVente) {
        this.idVente = idVente;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getDatVente() {
        return datVente;
    }

    public void setDatVente(Date datVente) {
        this.datVente = datVente;
    }

    public List<LigneVente> getLigneVentes() {
        return ligneVentes;
    }

    public void setLigneVentes(List<LigneVente> ligneVentes) {
        this.ligneVentes = ligneVentes;
    }
}
