package cnps.dsi.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class LigneVente implements Serializable {

    @Id
    @GeneratedValue
    private Long idLigneVente;

    @ManyToOne
    @JoinColumn(name = "idArticle")
    private Article article;

    @ManyToOne
    @JoinColumn(name = "idVente")
    private Vente vente;

    public Long getIdLigneVente() {
        return idLigneVente;
    }

    public void setIdLigneVente(Long idLigneVente) {
        this.idLigneVente = idLigneVente;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Vente getVente() {
        return vente;
    }

    public void setVente(Vente vente) {
        this.vente = vente;
    }
}
