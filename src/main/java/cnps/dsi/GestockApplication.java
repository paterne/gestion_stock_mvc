package cnps.dsi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestockApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestockApplication.class, args);
	}

}
